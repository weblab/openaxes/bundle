#!/bin/bash

# Defining the variables 
LOG_FILE=celery.$(date +"%Y_%m_%d").out
PID_FILE=celery.pid


# Defining the functions
startCelery() {
   if [[ ! -f $MANAGE_PY_FILE ]] ; then
      echo "File $MANAGE_PY_FILE does not exist."
      exit 80
   fi
   if [ $(getCeleryPID) -ne 0 ] ; then
      echo "Celery is still running. Unable to start it."
      exit 85
   fi
   nohup python $MANAGE_PY_FILE celery worker  >$LOG_FILE 2>&1 &
   # TODO Try to guess if it is really started (parsing the logs?)
   echo $! > $PID_FILE
   echo "Celery started."
}

stopCelery() {
   local pid=$(getCeleryPID)
   if [[ $pid -eq 0 ]] ; then
      echo "Celery is not running. Nothing to do."
   else
      echo "Stopping Celery."
      kill $pid
      sleep 3s
      rm $PID_FILE
   fi
}

getCeleryPID() {
   local pid="0"
   if [ -f $PID_FILE ]; then
      pid=$(cat $PID_FILE 2>/dev/null)
      if [[ $? -eq 0 ]]; then
         ps -p $pid >/dev/null 2>&1
         if [[ $? -ne 0 ]]; then
            pid="0"
            rm $PID_FILE
         fi
      else
         rm $PID_FILE
      fi
   fi
   if [ -z $pid ]; then
      pid="0"
   fi
   echo $pid
}


# The script really starts here!
if [[ $# -lt 1 ]]; then
   echo "No command provided. Possible values are start/stop/status/restart"
   exit 90
fi
COMMAND=$1
shift


if [[ $# -lt 1 ]]; then
   echo "No path to manage.py provided. It must be the second argument."
   exit 95
fi
MANAGE_PY_FILE=$1
shift



case $COMMAND in
  'start')
      echo "Starting Celery."
      startCelery
   ;;
   'stop')
      echo "Stopping Celery."
      stopCelery
   ;;
   'restart')
      echo "Restarting Celery."
      stopCelery
      startCelery
   ;;
   'status')
      echo "Getting Celery status."
      pid=$(getCeleryPID)
      if [ $pid -eq 0 ] ; then
         echo "Celery not running."
      else
         echo "Celery is running on PID $pid."
      fi
   ;;
   *)
       echo "Unsupported command $COMMAND."
       echo "Supported one are: start, stop, restart and status."
       exit 105
   ;;
esac
