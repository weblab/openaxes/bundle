#!/bin/bash
set -e
###################################################################################################
#################################### Open AXES Install Script #####################################
###################################################################################################
#                                                                                                 #
# This script is in charge of:                                                                    #
#    - installing required packages like Java, Python, Apache...                                  #
#    - configuring the new instance of Apache w.r.t. the need for Open AXES GUI                   #
#    - compiling a local version of FFmpeg since a specific version is needed for shot-detection  #
#    - downloading MCR and classifier as well as installing them if activated when calling script #
#                                                                                                 #
# Options:                                                                                        #
#    --includeClassifier, can be used to also install Matlab CR and classifiers.                  #
#                           This requires one more space (1 Gigabyte) and takes more time for the #
#                           installation. Processing time will also be increased.                 #
#    --includeOnTheFlyClassifier, can be used to also install cpu-visor system which is able to   #
#                           learn new classifier on the fly via request on external services like #
#                           Google or Bing image search. This requires more space, install a lot  #
#                           of development libraries since compilation has to be done. As a       #
#                           result installation time will be widely increased.                    #
#    --includeFFmpegGPL, can be used to compile FFmpeg with --enable-gpl --enable-libx264 flags.  #
#                           This is needed in order to process H264 videos. Due to licence issue, #
#                           this is not activated by default.                                     #
#    --nonInteractiveMode, can be used to assume the answer is yes to any question and to extend  #
#                           sudo session in order to ease automatic installation using tools like #
#                           Vagrant, Puppet or Ansible (but the Java PPA based installation).     #
#                                                                                                 #
# NB: sudo -v is used often to ensure that the sudo rights are valid along the whole script.      #
###################################################################################################



###################################################################################################
################################# First check options #############################################
###################################################################################################

ffmpegFlags="--disable-logging --disable-doc --enable-shared --extra-cflags=-w --enable-libvorbis --enable-libvpx --enable-libopus --enable-static --enable-libass --enable-libmp3lame"
aptGetFlags=
pipFlags=
addAptRepositoryFlags=
makeFlags=
matlabInstallFlags=
unzipFlags=

while  [ $# -gt 0 ]; do
	case $1 in
		'--includeClassifier')
			includeClassifier=true;
		;;
		'--includeOnTheFlyClassifier')
			includeOnTheFlyClassifier=true;
		;;
		'--includeFFmpegGPL')
			ffmpegFlags="$ffmpegFlags --enable-gpl --enable-libx264"
		;;
		'--nonInteractiveMode')
			aptGetFlags="$aptGetFlags -q -y"
			extendSudo=true
			pipFlags="$pipFlags -q"
			addAptRepositoryFlags="$addAptRepositoryFlags -y"
			makeFlags="$makeFlags -s"
			matlabInstallFlags="$matlabInstallFlags -mode silent -agreeToLicense yes"
			unzipFlags="$unzipFlags -o -q"
		;;
		*)
			echo "Parameter $1 is not handled."
			exit 80
		;;
	esac
	shift
done



###################################################################################################
###################################### Packages installation ######################################
###################################################################################################


# Add PPA for Oracle Java if not already there.
if ! grep -q "^deb http://ppa.launchpad.net/webupd8team/java/ubuntu" /etc/apt/sources.list /etc/apt/sources.list.d/*.list; then
	echo "Installing a personal package archive for Oracle JDK (see http://ubuntuhandbook.org/index.php/2014/02/install-oracle-java-6-7-or-8-ubuntu-14-04/ for more details)."
	sudo add-apt-repository $addAptRepositoryFlags ppa:webupd8team/java
fi



sudo apt-get $aptGetFlags update
if [[ -n "$extendSudo" ]] ; then sudo -v ; fi
echo "Installing needed packages through APT-GET"
# Java for all
sudo apt-get $aptGetFlags install oracle-java7-installer
if [[ -n "$extendSudo" ]] ; then sudo -v ; fi
# Python and Apache for the UI
sudo apt-get $aptGetFlags install python-pip python-dev apache2 libapache2-mod-wsgi
if [[ -n "$extendSudo" ]] ; then sudo -v ; fi
# Various lib needed for FFMPEG, UI Virtual cutter, video-normaliser and shot extractor
sudo apt-get $aptGetFlags install autoconf automake build-essential libass-dev libbz2-dev libfreetype6-dev libgpac-dev libjpeg8 libmp3lame-dev
sudo apt-get $aptGetFlags install libopus-dev libsdl1.2-dev libtheora-dev libtool libva-dev libvdpau-dev libvorbis-dev libvpx-dev libx264-dev
sudo apt-get $aptGetFlags install libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev pkg-config texi2html yasm zlib1g-dev
if [[ -n "$extendSudo" ]] ; then sudo -v ; fi
# Various lib needed pastec
sudo apt-get $aptGetFlags install libopencv-dev libmicrohttpd-dev libjsoncpp-dev curl libyaml-dev
if [[ -n "$extendSudo" ]] ; then sudo -v ; fi
# Various lib needed for Visor
if [[ -n "$includeOnTheFlyClassifier" ]] ; then
	sudo apt-get $aptGetFlags install build-essential cmake libopencv-dev libgoogle-glog-dev libbz2-dev sed gawk gfortran python-numpy
	if [[ -n "$extendSudo" ]] ; then sudo -v ; fi
	sudo apt-get $aptGetFlags install libprotobuf-dev protobuf-compiler libsnappy1 libsnappy-dev libleveldb1 libleveldb-dev
	if [[ -n "$extendSudo" ]] ; then sudo -v ; fi
	sudo apt-get $aptGetFlags install libopenblas-dev liblmdb-dev libzmq-dev libssl-dev hdf5-tools libhdf5-dev liblapack-dev
fi
echo "APT-GET based installation finished successfully"


echo "Installing needed Python packages through PIP"
sudo pip $pipFlags install ipython django pymongo requests jsonrpclib pyyaml celery django-celery
if [[ -n "$extendSudo" ]] ; then sudo -v ; fi
sudo pip $pipFlags install PIL --allow-external PIL --allow-unverified PIL
# A few more libs needed for cpuvisor and imsearch-tools to be installed through PIP
if [[ -n "$includeOnTheFlyClassifier" ]] ; then
	sudo pip $pipFlags install "numpy>=1.8.0" "matplotlib>=1.3.1" "pyzmq>=14.3.1" "gevent>=1.0.1" "protobuf>=2.5.0" scipy flask gevent-zeromq
fi
echo "PIP based installation finished successfully"



###################################################################################################
############# Configuring the GUI/LIMAS with appropriate links and permissions ####################
###################################################################################################


echo "Configuring GUI/LIMAS paths and permissions"

if [[ -n "$extendSudo" ]] ; then sudo -v ; fi
sed 's?BUNDLELOCATION?'`pwd`'?' conf/gui/axes-research.conf.tmpl > conf/gui/axes-research.conf
sudo ln -sf "$(pwd)/conf/gui/axes-research.conf" /etc/apache2/sites-enabled/axes-research.conf

sed 's?FFMPEG_PATH?'$(pwd)/components/ffmpeg-1.1.16/ffmpeg.sh'?' conf/gui/defaults.py.tmpl > conf/gui/defaults.py.tmpl1
sed 's?BUNDLEVERSION?'1.1.0'?' conf/gui/defaults.py.tmpl1 > conf/gui/defaults.py.tmpl2
if [[ -n "$includeOnTheFlyClassifier" ]] ; then
	sed 's?VISUAL_SEARCH_ACTIVATED?'\'category-t\''?' conf/gui/defaults.py.tmpl2 > conf/gui/defaults.py
elif [[ -n "$includeClassifier" ]] ; then
    sed 's?VISUAL_SEARCH_ACTIVATED?'\'category-t\''?' conf/gui/defaults.py.tmpl2 > conf/gui/defaults.py
else
	sed 's?VISUAL_SEARCH_ACTIVATED?''?' conf/gui/defaults.py.tmpl2 > conf/gui/defaults.py
fi
mv -f conf/gui/defaults.py axes-research-0.8.1/axesresearch/settings/
cp conf/gui/overriding/wsgi.py axes-research-0.8.1/axesresearch/
cp conf/gui/overriding/local.py axes-research-0.8.1/axesresearch/settings/
cp conf/gui/overriding/AXES.png axes-research-0.8.1/axesresearch/ui/static/img/AXESresearch.png
cp conf/gui/overriding/model.js axes-research-0.8.1/axesresearch/ui/static/js/
cp conf/gui/overriding/index.html axes-research-0.8.1/axesresearch/ui/templates/
cp conf/gui/overriding/login.html axes-research-0.8.1/axesresearch/ui/templates/
cp conf/gui/overriding/search.html axes-research-0.8.1/axesresearch/ui/templates/views/
cp conf/gui/overriding/filters.html axes-research-0.8.1/axesresearch/ui/templates/views/
mkdir -p axes-research-0.8.1/logs

sudo mkdir -p /var/www/html/axes-research/
mkdir -p data/content-manager
mkdir -p data/toIndex
mkdir -p data/mongo

touch axes-research-0.8.1/logs/axesresearch.log

if [[ -n "$extendSudo" ]] ; then sudo -v ; fi
sudo ln -nsf "$(pwd)/data/gui/media" /var/www/html/axes-research/media
sudo ln -nsf "$(pwd)/axes-research-0.8.1/axesresearch/ui/static" /var/www/html/axes-research/static
sudo ln -nsf "$(pwd)/data/content-manager" /var/www/html/exposed-content
sudo ln -nsf "$(pwd)/data/content-manager" /var/www/html/keyframes

# TODO See how to fix OPENAXES-136 here. Gui files should be rw for apache user. Content-manager and mongo for both apache and openaxes
sudo chmod 777 -Rf data/content-manager/
sudo chmod 777 -Rf data/gui/
sudo chmod 777 -Rf axes-research-0.8.1/
sudo chmod 777 -Rf data/mongo/

if [[ -n "$extendSudo" ]] ; then sudo -v ; fi
sudo service apache2 restart

echo "GUI and LIMAS configuration finished successfully"



###################################################################################################
###################################### Installing FFMPEG ##########################################
###################################################################################################


cd components
echo "Installing FFMPEG with flags $ffmpegFlags"

# Version 1.1.x is needed to be compliant with ShotDetector and KeyFrameExtractor.
# Normaliser could use recent one but once this one is installed and needed, the normaliser also use this one.
tar -zxf ffmpeg-1.1.16.tar.gz

cd ffmpeg-1.1.16
./configure $ffmpegFlags
make -j -s

# Wrapping ffmpeg binary in order to set its library path
LD_LIBRARY_PATH=""
for dir in lib*/; do 
        cd $dir;
        LD_LIBRARY_PATH=$LD_LIBRARY_PATH`pwd`:
        cd ..
done
if [[ -f ffmpeg.sh ]]; then
	rm ffmpeg.sh
fi
echo "#!/bin/bash" >> ffmpeg.sh
echo "export LD_LIBRARY_PATH=\"$LD_LIBRARY_PATH\"" >> ffmpeg.sh
echo "./ffmpeg \"\$@\"" >> ffmpeg.sh
chmod +x ffmpeg.sh
cd ..
echo "FFMPEG successfully installed"



###################################################################################################
###################################### Installing Pastec ##########################################
###################################################################################################


cd pastec
echo "Installing Pastec"
tar zxf visualWordsORB.tar.gz
chmod +x pastec
chmod +x pastec.sh
cd ..
echo "Pastec successfully installed"



###################################################################################################
######################### Installing MCR and classifiers if required ##############################
###################################################################################################


if [[ -n "$includeClassifier" ]] ; then
	echo "Installing Matlab Compiler Runtime. This may take a while due to both download and installation."
	mkdir -p MCR/2014a
	cd MCR/2014a
	wget --continue http://maven.ercim.eu/axes/com/mathworks/MCR/R2014a/MCR-R2014a-glnxa64.zip
	unzip $unzipFlags MCR-R2014a-glnxa64.zip
	./install $matlabInstallFlags -destinationFolder "$(pwd)"

	echo "Matlab Compiler Runtime successfully installed"
	echo "Installing KUL Ueberclassifiers. This may take a while due to download."

	cd ../..
	mkdir -p KUL
	cd KUL
	wget --continue http://maven.ercim.eu/axes/eu/axes/resources/ueberclass_leuven/ueberclass1537_leuven_v2014a/ueberclass_leuven-ueberclass1537_leuven_v2014a.zip
	unzip $unzipFlags ueberclass_leuven-ueberclass1537_leuven_v2014a.zip
	mkdir -p ueberclass1537_leuven_v2014a/1537class/collections
	echo "KUL Ueberclassifiers successfully installed"
	cd ..
fi


###################################################################################################
####################### Installing Visor (and dependencies) if required  ##########################
###################################################################################################


if [[ -n "$includeOnTheFlyClassifier" ]] ; then

	echo "Downloading, compiling and installing CPU Visor and its dependencies."
	cd UO

	echo "Downloading and unzipping cpuvisor."
	wget --continue http://maven.ercim.eu/axes/eu/axes/visor/cpuvisor-srv/0.2.3/cpuvisor-srv-0.2.3.zip
	unzip $unzipFlags cpuvisor-srv-0.2.3.zip
	cd cpuvisor-srv-0.2.3
	rm -r imsearch-tools
	PREFIX=$(pwd)
	mkdir -p {lib,include,build,server_data,model_data}


	echo "Downloading and unzipping imsearch-tool"
	wget --continue http://maven.ercim.eu/axes/eu/axes/visor/imsearch-tools/1.2.1/imsearch-tools-1.2.1-8c43d9b643b5d8372be8e2147229388505528031.zip
	unzip $unzipFlags imsearch-tools-1.2.1-8c43d9b643b5d8372be8e2147229388505528031.zip
	mv imsearch-tools-master imsearch-tools


	echo "Downloading and compiling Boost."
	wget --continue http://maven.ercim.eu/axes/org/boost/boost/1.57.0/boost-1.57.0.tar.gz
	tar xzf boost-1.57.0.tar.gz
	cd boost_1_57_0
	./bootstrap.sh
	./b2 -q install --prefix=$PREFIX
	cd ..


	echo "Downloading and compiling LibLinear"
	wget --continue http://maven.ercim.eu/axes/tw/edu/ntu/csie/liblinear/1.96/liblinear-1.96.tar.gz
	tar xzf liblinear-1.96.tar.gz
	cd liblinear-1.96
	make
	make lib
	install -m 755 linear.h $PREFIX/include
	install -m 755 liblinear.so.2 $PREFIX/lib
	ln -sf $PREFIX/lib/liblinear.so.2 $PREFIX/lib/liblinear.so
	cd ..


	echo "Downloading and compiling GFlags"
	wget --continue http://maven.ercim.eu/axes/eu/axes/visor/gflags/2.1.1/gflags-2.1.1.zip
	unzip $unzipFlags gflags-2.1.1.zip
	cd gflags-2.1.1
	mkdir -p build
	cd build
	cmake -D CMAKE_INSTALL_PREFIX=$PREFIX -D CMAKE_CXX_FLAGS=-fPIC ../
	make -j
	make install -j
	cd ../..


	echo "Downloading and compiling Caffe"
	wget --continue http://maven.ercim.eu/axes/eu/axes/visor/caffe-dev/8f0c71cb86daaf753ea812be267fbb95b69aeabe/caffe-dev-8f0c71cb86daaf753ea812be267fbb95b69aeabe.zip
	unzip $unzipFlags caffe-dev-8f0c71cb86daaf753ea812be267fbb95b69aeabe.zip
	cd caffe-dev
	cp Makefile.config.example Makefile.config
	sed -i -e "s#INCLUDE_DIRS := \$(PYTHON_INCLUDE) /usr/local/include#INCLUDE_DIRS := \$(PYTHON_INCLUDE) /usr/local/include $PREFIX/include /usr/include/hdf5/serial/#" Makefile.config
	sed -i -e "s#LIBRARY_DIRS := \$(PYTHON_LIB) /usr/local/lib /usr/lib#LIBRARY_DIRS := \$(PYTHON_LIB) /usr/local/lib /usr/lib $PREFIX/lib /usr/lib/x86_64-linux-gnu/hdf5/serial#" Makefile.config
	sed -i -e "s/# CPU_ONLY := 1/CPU_ONLY := 1/" Makefile.config
	sed -i -e "s/BLAS := atlas/BLAS := open/" Makefile.config
	make all -j
	install -m 0755 build/lib/libcaffe.so $PREFIX/lib
	rm -rf $PREFIX/include/caffe
	cp -r include/caffe $PREFIX/include/caffe
	mkdir -p $PREFIX/include/caffe/proto/
	cp -r build/src/caffe/proto/*.h $PREFIX/include/caffe/proto
	cd ..
	echo "Caffe successfully installed."


	echo "Downloading and compiling CPP NetLib"
	wget --continue http://maven.ercim.eu/axes/eu/axes/visor/cpp-netlib/0.11-devel/cpp-netlib-0.11-devel-f006a799d382c048d9a1a3a33608ed197f96a812.zip
	unzip $unzipFlags cpp-netlib-0.11-devel-f006a799d382c048d9a1a3a33608ed197f96a812.zip
	cd cpp-netlib-0.11-devel
	# Fixing https://github.com/cpp-netlib/cpp-netlib/issues/381
	awk 'NR==8{print "set (CMAKE_CXX_FLAGS \"${CMAKE_CXX_FLAGS} --param ggc-min-expand=20\")"; next}1' CMakeLists.txt > CMakeLists.txt.new
	mv CMakeLists.txt CMakeLists.txt.bak
	mv CMakeLists.txt.new CMakeLists.txt
	mkdir -p build
	cd build
	cmake -D CMAKE_INSTALL_PREFIX=$PREFIX ../
	make
	make install
	cd ../..
	echo "CPP NetLib successfully installed."


	cat > setup_env.sh <<END_OF_MESSAGE_HERE
export PATH=$PREFIX/bin:\$PATH
export CPLUS_INCLUDE_PATH=$PREFIX/include:/usr/include/hdf5/serial/:\$CPLUS_INCLUDE_PATH
export LD_LIBRARY_PATH=$PREFIX/lib:$PREFIX/lib64:/usr/lib/x86_64-linux-gnu/hdf5/serial:\$LD_LIBRARY_PATH
export LIBRARY_PATH=$PREFIX/lib:$PREFIX/lib64:\$LIBRARY_PATH
export BOOST_ROOT=$PREFIX
export CMAKE_PREFIX_PATH=$PREFIX
export MKL_NUM_THREADS=1
END_OF_MESSAGE_HERE


	echo "Compiling CPU Visor it-self"
	# Fixing OPENAXES-177 which is solved on CPUVisor in a branch of the next version not compatible with current in use here
	cp ../preproc.cc src/server/util/preproc.cc
	source setup_env.sh
	cd build
	cmake ../
	make
	make install
	cd ..


	echo "Configuring Visor."
	rm config.prototxt
	sed 's?<BASE_DIR>?'$(pwd)'?' ../config.prototxt.tmpl > config.prototxt
	rm dsetpaths.txt
	touch dsetpaths.txt


	echo "Downloading pretrained features models."
	cd server_data
	wget --continue http://maven.ercim.eu/axes/eu/axes/visor/server_data/0.2.3/server_data-0.2.3.tar
	tar xf server_data-0.2.3.tar
	# In fact just an empty file is needed here since the dataset will be enriched incrementaly by the visor-indexing service.
	rm dsetfeats.binaryproto
	touch dsetfeats.binaryproto
	cd ..


	#echo "Downloading negative images used when learning models."
	#cd server_data
	#mkdir -p neg_images
	#cd neg_images
	#wget --continue http://maven.ercim.eu/axes/eu/axes/visor/neg_images/0.2.3/neg_images-0.2.3.tar
	#tar xf neg_images-0.2.3.tar
	#cd ../..


	echo "Downloading some pretrained features models."
	cd model_data
	wget --continue http://maven.ercim.eu/axes/eu/axes/visor/VGG_CpuVisor_Conf/0.2.3/VGG_CpuVisor_Conf-0.2.3.zip
	unzip $unzipFlags VGG_CpuVisor_Conf-0.2.3.zip


	cd ../..
fi


echo "Installation is finished"
echo "Please start OpenAXES by calling: ./openaxes.sh start"
echo "You can then access OpenAXES on http://localhost/openaxes using the login 'axes' and password 'axes' " 
