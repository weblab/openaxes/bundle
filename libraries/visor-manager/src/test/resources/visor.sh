#!/bin/bash

# Defining the variables 
LOG_FILE=$(pwd)/visor.$(date +"%Y_%m_%d").out
PID_FILE=$(pwd)/visor.pid



# Defining the functions
startVisor() {
   if [ $(getVisorPID) -ne 0 ] ; then
      echo "Visor is still running. Unable to start it."
      exit 85
   fi
   cd $VISOR_DIRECTORY
   source $SOURCE_SCRIPT
   cd bin
   nohup $START_SCRIPT >$LOG_FILE 2>&1 &
   # TODO Try to guess if it is really started (parsing the logs?)
   echo $! > $PID_FILE
   echo "Visor started."
}


stopVisor() {
   local pid=$(getVisorPID)
   if [[ $pid -eq 0 ]] ; then
      echo "Visor is not running. Nothing to do."
   else
      echo "Stopping Visor."
      kill $pid
      sleep 3s
      rm $PID_FILE
   fi
}


getVisorPID() {
   local pid="0"
   if [ -f $PID_FILE ]; then
      pid=$(cat $PID_FILE 2>/dev/null)
      if [[ $? -eq 0 ]]; then
         ps -p $pid >/dev/null 2>&1
         if [[ $? -ne 0 ]]; then
            pid="0"
            rm $PID_FILE
         fi
      else
         rm $PID_FILE
      fi
   fi
   if [ -z $pid ]; then
      pid="0"
   fi
   echo $pid
}


# The script really starts here!
if [[ $# -lt 1 ]]; then
   echo "No command provided. Possible values are start/stop/status/restart"
   exit 90
fi
COMMAND=$1
shift


case $COMMAND in
  'start')
      echo "Starting Visor."
      if [[ $# -lt 1 ]]; then
 	     echo "No path to Visor directory provided. It must be the second argument."
	     exit 95
      fi
      VISOR_DIRECTORY=$1
      shift
      if [[ $# -lt 1 ]]; then
 	     echo "No name of source script provided. It must be the third argument."
	     exit 95
      fi
      SOURCE_SCRIPT=$1
      shift
      if [[ $# -lt 1 ]]; then
 	     echo "No startup script provided. It must be the fourth argument."
	     exit 95
      fi
      START_SCRIPT=$1
      startVisor
   ;;
   'stop')
      echo "Stopping Visor."
      stopVisor
   ;;
   'status')
      echo "Getting Visor status."
      pid=$(getVisorPID)
      if [ $pid -eq 0 ] ; then
         echo "Visor not running."
      else
         echo "Visor is running on PID $pid."
      fi
   ;;
   *)
       echo "Unsupported command $COMMAND."
       echo "Supported one are: start, stop, restart and status."
       exit 105
   ;;
esac
