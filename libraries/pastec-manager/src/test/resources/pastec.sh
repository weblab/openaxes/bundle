#!/bin/bash

# Defining the variables 
LOG_FILE=pastec.$(date +"%Y_%m_%d").out
PID_FILE=pastec.pid
CODEBOOK_FILE=visualWordsORB.dat
HOST=localhost


# Defining the functions
startPastec() {
   if [[ ! -f $CODEBOOK_FILE ]] ; then
      echo "Code book file $CODEBOOK_FILE does not exist."
      exit 80
   fi
   if [ $(getPastecPID) -ne 0 ] ; then
      echo "Pastec is still running. Unable to start it."
      exit 85
   fi
   nohup ./pastec -p $PORT $CODEBOOK_FILE >$LOG_FILE 2>&1 &
   # TODO Try to guess if it is really started (parsing the logs?)
   echo $! > $PID_FILE
   echo "Pastec started."
}

stopPastec() {
   local pid=$(getPastecPID)
   if [[ $pid -eq 0 ]] ; then
      echo "Pastec is not running. Nothing to do."
   else
      echo "Pastec is running. Trying to serialise index before stopping."
      curl -X POST -d '{"type":"WRITE"}' http://$HOST:$PORT/index/io
      # TODO Parse the output to check if the index has really been written
      echo "Index written. Now stopping."
      kill $pid
      rm $PID_FILE
   fi
}

getPastecPID() {
   local pid="0"
   if [ -f $PID_FILE ]; then
      pid=$(cat $PID_FILE 2>/dev/null)
      if [[ $? -eq 0 ]]; then
         ps -p $pid >/dev/null 2>&1
         if [[ $? -ne 0 ]]; then
            pid="0"
            rm $PID_FILE
         fi
      else
         rm $PID_FILE
      fi
   fi
   if [ -z $pid ]; then
      pid="0"
   fi
   echo $pid
}


# The script really starts here!
if [[ $# -lt 1 ]]; then
   echo "No command provided. Possible values are start/stop/status/restart"
   exit 90
fi
COMMAND=$1
shift


if [[ $# -lt 1 ]]; then
   echo "No port provided. The second argument must be the port of Pastec."
   exit 95
fi
PORT=$1
shift


if [[ $# -gt 0 ]]; then
   CODEBOOK_FILE=$1
   shift 100
fi



case $COMMAND in
  'start')
      echo "Starting Pastec."
      startPastec
   ;;
   'stop')
      echo "Stopping Pastec."
      stopPastec
   ;;
   'restart')
      echo "Restarting Pastec."
      stopPastec
      startPastec
   ;;
   'status')
      echo "Getting Pastec status."
      pid=$(getPastecPID)
      if [ $pid -eq 0 ] ; then
         echo "Pastec not running."
      else
         echo "Pastec is running on PID $pid."
      fi
   ;;
   *)
       echo "Unsupported command $COMMAND."
       echo "Supported one are: start, stop, restart and status."
       exit 105
   ;;
esac
