/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package eu.axes.bundle.server;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import javax.management.MBeanServerConnection;

import org.apache.commons.io.FileUtils;
import org.ow2.weblab.bundle.server.WebLabServer;
import org.ow2.weblab.bundle.utils.ProcessUtils;
import org.ow2.weblab.bundle.utils.Utils;

/**
 * An specific implementation of WebLab server to be used in order to manage a Pastec server into the main script.
 *
 * @date 2015-01-30
 * @author ymombrun
 */
public class Pastec extends WebLabServer {


	private static final Set<String> ALLOWED_COMMANDS = new HashSet<>(Arrays.asList("start", "stop", "status", "restart"));


	private static final int DEFAULT_PORT = 4212;


	private static final String PID_FILE = "pastec.pid";


	/**
	 * The codebook word file.
	 */
	private String wordFile;


	/**
	 * Just an empty constructor that says that the manager has been created.
	 */
	public Pastec() {
		super();
		this.logger.finest("Pastec Manager started.");
	}


	@Override
	public String getProcessIdentificationClue() {
		this.logger.finest("getProcessIdentificationClue is not implemented by Pastec Manager.");
		return this.getHome();
	}


	@Override
	public boolean isServerFullyStarted(final boolean showDetails) {
		if (!this.isEnabled()) {
			this.logger.info("Can not check if " + this.getName() + " is started since control is disabled.");
			return true;
		}
		this.logger.info("Checking " + this.getName() + " status... ");
		return (this.status() == State.STARTED) && !Utils.isPortAvailable(this.logger, this.getPort());
	}


	/**
	 * Execute a command.
	 *
	 * @param command
	 *            The command to be executed
	 * @return And instance of process
	 */
	public Process execute(final String command) {
		if (!this.isEnabled()) {
			this.logger.info(this.getName() + " discarded command " + command + " since control is disabled.");
			return ProcessUtils.dummyProccess();
		}
		if (Pastec.ALLOWED_COMMANDS.contains(command)) {
			this.logger.warning("Command " + command + " is not handled by " + this.getName() + ".");
			return null;
		}
		if (!this.checkScript()) {
			return null;
		}
		if ("start".equals(command)) {
			return this.start();
		}
		if ("status".equals(command)) {
			this.status();
			return ProcessUtils.dummyProccess();
		}
		if ("stop".equals(command)) {
			this.stop();
			return ProcessUtils.dummyProccess();
		}
		if ("restart".equals(command)) {
			this.stop();
			return this.start();
		}
		return this.execute(this.getEnv(), command);
	}


	@Override
	public MBeanServerConnection getMBeanServerConnection() {
		this.logger.fine("MBeanServerConnection is not handled for server " + this.getName() + ".");
		return null;
	}


	@Override
	public Process start() {
		if (this.status() != State.STOPPED) {
			this.logger.severe(this.getName() + " is not stopped! Aborting start...");
			return null;
		}

		if (ProcessUtils.checkPidFile(this.logger, this, false)) {
			this.logger.severe(this.getName() + " already started! Aborting...");
			return null;
		}

		this.logger.info(this.getName() + " is starting...");

		final Process p = ProcessUtils.runProcess(this.logger, this.getBinDirectory(), this.getEnv(), this.getScript(), "start", String.valueOf(this.getPort()), this.getWordFile());
		final int exitCode;
		try {
			exitCode = p.waitFor();
		} catch (final InterruptedException ie) {
			this.logger.info(this.getName() + " start failed.");
			return null;
		}
		if ((exitCode == 0) && (this.status() == State.STARTED)) {
			this.logger.info(this.getName() + " is started.");
		} else {
			this.logger.info(this.getName() + " start failed.");
			return null;
		}
		return p;
	}


	@Override
	public void stop() {
		this.logger.info(this.getName() + " is stopping...");
		ProcessUtils.runProcess(this.logger, this.getBinDirectory(), this.getEnv(), this.getScript(), "stop", String.valueOf(this.getPort()), this.getWordFile());
		this.logger.info(this.getName() + " is stopped.");
	}


	@Override
	public String supportedCommands() {
		return this.getName() + " [start|stop|restart|status] \t will start, stop, restart or retrieve status information about server " + this.getName() + ".\n";
	}


	/**
	 * @return Return <code>true</code> if bin directory and script exists and can be executed.
	 */
	private boolean checkScript() {
		final File binDir = new File(this.getBinDirectory());
		if (!binDir.exists()) {
			this.logger.severe("binDirectory " + binDir.getAbsolutePath() + " does not exists... Unable to run start command inside.");
			return false;
		}
		if (!binDir.isDirectory()) {
			this.logger.severe("binDirectory " + binDir.getAbsolutePath() + " is not a directory... Unable to run start command inside.");
			return false;
		}
		final File script = new File(binDir, this.getScript());
		if (!script.exists()) {
			this.logger.severe("script " + script.getAbsolutePath() + " does not exists... Unable to run it to start server.");
			return false;
		}
		if (!script.isFile()) {
			this.logger.severe("script " + script.getAbsolutePath() + " is not a file... Unable to run it to start server.");
			return false;
		}
		if (!script.canExecute()) {
			this.logger.severe("script " + script.getAbsolutePath() + " is not executable... Unable to run it to start server.");
			return false;
		}
		if (this.wordFile != null) {
			final File wordFileFile = new File(binDir, this.wordFile);
			if (!wordFileFile.exists()) {
				this.logger.severe("wordFile " + wordFileFile.getAbsolutePath() + " does not exists... Unable to use it to start server.");
				return false;
			}
			if (!wordFileFile.isFile()) {
				this.logger.severe("wordFile " + wordFileFile.getAbsolutePath() + " is not a file... Unable to use it to start server.");
				return false;
			}
			if (!wordFileFile.canRead()) {
				this.logger.severe("wordFile " + wordFileFile.getAbsolutePath() + " is not readable... Unable to use it to start server.");
				return false;
			}
		}
		return true;
	}


	/**
	 * @return the wordFile
	 */
	public String getWordFile() {
		if (this.wordFile == null) {
			return "";
		}
		return this.wordFile;
	}


	/**
	 * @param wordFile
	 *            the wordFile to set
	 */
	public void setWordFile(final String wordFile) {
		this.wordFile = wordFile;
	}


	@Override
	public int getPort() {
		if (super.getPort() == 0) {
			return Pastec.DEFAULT_PORT;
		}
		if (super.getPort() == -1) {
			return Pastec.DEFAULT_PORT;
		}
		return super.getPort();
	}


	@Override
	public int getPid() {
		final File pidFile = new File(this.getBinDirectory(), Pastec.PID_FILE);
		if (pidFile.exists() && pidFile.isFile() && pidFile.canRead() && (pidFile.length() > 0)) {
			try {
				final String pidStr = FileUtils.readFileToString(pidFile).trim();
				return Integer.parseInt(pidStr);
			} catch (final Exception e) {
				this.logger.log(Level.WARNING, "Unable to read pidFile " + pidFile + ".", e);
				return -1;
			}
		}
		return -1;
	}

}
