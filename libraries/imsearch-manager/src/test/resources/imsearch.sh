#!/bin/bash

# Defining the variables 
LOG_FILE=$(pwd)/imsearch.$(date +"%Y_%m_%d").out
PID_FILE=$(pwd)/imsearch.pid



# Defining the functions
startImsearch() {
   if [ $(getImsearchPID) -ne 0 ] ; then
      echo "Imsearch is still running. Unable to start it."
      exit 85
   fi
   cd $IMSEARCH_DIRECTORY
   nohup python imsearch_http_service.py $PORT >$LOG_FILE 2>&1 &
   # TODO Try to guess if it is really started (parsing the logs?)
   echo $! > $PID_FILE
   echo "Imsearch started."
}


stopImsearch() {
   local pid=$(getImsearchPID)
   if [[ $pid -eq 0 ]] ; then
      echo "Imsearch is not running. Nothing to do."
   else
      echo "Stopping Imsearch."
      kill $pid
      sleep 3s
      rm $PID_FILE
   fi
}


getImsearchPID() {
   local pid="0"
   if [ -f $PID_FILE ]; then
      pid=$(cat $PID_FILE 2>/dev/null)
      if [[ $? -eq 0 ]]; then
         ps -p $pid >/dev/null 2>&1
         if [[ $? -ne 0 ]]; then
            pid="0"
            rm $PID_FILE
         fi
      else
         rm $PID_FILE
      fi
   fi
   if [ -z $pid ]; then
      pid="0"
   fi
   echo $pid
}


# The script really starts here!
if [[ $# -lt 1 ]]; then
   echo "No command provided. Possible values are start/stop/status/restart"
   exit 90
fi
COMMAND=$1
shift


case $COMMAND in
  'start')
      echo "Starting Imsearch."
      if [[ $# -lt 1 ]]; then
 	     echo "No path to Imsearch directory provided. It must be the second argument."
	     exit 95
      fi
      IMSEARCH_DIRECTORY=$1
      shift
      if [[ $# -lt 1 ]]; then
 	     echo "No port provided. It must be the third argument."
	     exit 95
      fi
      PORT=$1
      startImsearch
   ;;
   'stop')
      echo "Stopping Imsearch."
      stopImsearch
   ;;
   'status')
      echo "Getting Imsearch status."
      pid=$(getImsearchPID)
      if [ $pid -eq 0 ] ; then
         echo "Imsearch not running."
      else
         echo "Imsearch is running on PID $pid."
      fi
   ;;
   *)
       echo "Unsupported command $COMMAND."
       echo "Supported one are: start, stop, restart and status."
       exit 105
   ;;
esac
