/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package eu.axes.bundle.server;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

import javax.management.MBeanServerConnection;

import org.apache.commons.io.FileUtils;
import org.ow2.weblab.bundle.server.WebLabServer;
import org.ow2.weblab.bundle.utils.ProcessUtils;
import org.ow2.weblab.bundle.utils.Utils;

/**
 * An specific implementation of WebLab server to be used in order to manager a new server into the main script.
 *
 * @date 2014-10-14
 * @author ymombrun
 */
public class Mongo extends WebLabServer {


	private static final String MONGOD_LOCK = "mongod.lock";


	private static final String DEFAULT_DB_PATH = "/data/db";


	private String dbpath;


	private String logpath;


	/**
	 * Just an empty constructor that says that the manager has been created.
	 */
	public Mongo() {
		super();
		this.logger.finest("Mongo Manager started.");
	}


	@Override
	public String getProcessIdentificationClue() {
		return this.getHome();
	}


	@Override
	public boolean isServerFullyStarted(final boolean showDetails) {
		if (!this.isEnabled()) {
			this.logger.info("Can not check if " + this.getName() + " is started since control is disabled.");
			return true;
		}
		this.logger.info("Checking " + this.getName() + " status... ");
		return this.status() == State.STARTED && !Utils.isPortAvailable(this.logger, this.getPort());
	}


	/**
	 * Execute a command.
	 * Currently only start, stop and restart are supported.
	 *
	 * @param command
	 *            The command to be executed
	 * @return And instance of process
	 */
	public Process execute(final String command) {
		if (!this.isEnabled()) {
			this.logger.info(this.getName() + " discarded command " + command + " since control is disabled.");
			return ProcessUtils.dummyProccess();
		}
		if ("start".equals(command)) {
			return this.start();
		}
		if ("restart".equals(command)) {
			this.stop();
			return this.start();
		}
		if ("status".equals(command)) {
			this.status();
			return ProcessUtils.dummyProccess();
		}
		if ("stop".equals(command)) {
			this.stop();
			return ProcessUtils.dummyProccess();
		}
		this.logger.warning("Command " + command + " is not handled by " + this.getName() + ".");
		return null;
	}


	@Override
	public MBeanServerConnection getMBeanServerConnection() {
		this.logger.fine("MBeanServerConnection is not handled for server " + this.getName() + ".");
		return null;
	}


	@Override
	public Process start() {
		if (this.status() != State.STOPPED) {
			this.logger.severe(this.getName() + " is not stopped! Aborting start...");
			return null;
		}

		if (ProcessUtils.checkPidFile(this.logger, this, false) && this.isLocked()) {
			this.logger.severe(this.getName() + " already started! Aborting...");
			return null;
		}

		if (this.isLocked()) {
			this.logger.severe(this.getName() + " has not been stopped correctly. You should recover your data or manually remove file " + this.getLockFile() + " before retrying to start.");
			return null;
		}

		if (!this.isScriptExecutable()) {
			return null;
		}


		final List<String> commands = this.initCommandsList();
		commands.add("--fork");
		if (this.logpath == null) {
			commands.add("--syslog");
		} else {
			commands.add("--logpath");
			final File logDir = new File(this.logpath).getParentFile();
			if (!logDir.exists() && !logDir.mkdirs()) {
				this.logger.severe("logpath parent folder " + logDir.getAbsolutePath() + " does not exists or cannot be created... Unable to start " + this.getName() + ".");
				return null;
			}
			if (!logDir.isDirectory()) {
				this.logger.severe("logpath parent folder " + logDir.getAbsolutePath() + " is not a directory...  Unable to start " + this.getName() + ".");
				return null;
			}
			commands.add(this.logpath);
		}

		this.logger.info(this.getName() + " is starting ...");
		final Process p = ProcessUtils.runProcess(this.logger, this.getBinDirectory(), this.getEnv(), commands.toArray(new String[] {}));
		try {
			p.waitFor();
		} catch (final InterruptedException ie) {
			final String msg = "An error occured when starting " + this.getName() + ".";
			this.logger.severe(msg);
			this.logger.log(Level.FINE, msg, ie);
			return null;
		}
		if (p.exitValue() != 0) {
			final String msg = "An error occured when starting " + this.getName() + ". Exit code is " + p.exitValue() + ".";
			this.logger.severe(msg);
			return null;
		}
		final State state = this.status();
		if (state != State.STARTED) {
			final String msg = "Server " + this.getName() + " state is not started but " + state + " .";
			this.logger.severe(msg);
			return null;
		}
		this.logger.info(this.getName() + " is started.");
		return p;
	}


	private boolean isScriptExecutable() {
		final File binDir = new File(this.getBinDirectory());
		if (!binDir.exists()) {
			this.logger.severe("binDirectory " + binDir.getAbsolutePath() + " does not exists... Unable to run start command inside.");
			return false;
		}
		if (!binDir.isDirectory()) {
			this.logger.severe("binDirectory " + binDir.getAbsolutePath() + " is not a directory... Unable to run start command inside.");
			return false;
		}
		final File script = new File(binDir, this.getScript());
		if (!script.exists()) {
			this.logger.severe("script " + script.getAbsolutePath() + " does not exists... Unable to run it to start server.");
			return false;
		}
		if (!script.isFile()) {
			this.logger.severe("script " + script.getAbsolutePath() + " is not a file... Unable to run it to start server.");
			return false;
		}
		if (!script.canExecute()) {
			this.logger.severe("script " + script.getAbsolutePath() + " is not executable... Unable to run it to start server.");
			return false;
		}
		return true;
	}


	private boolean isLocked() {
		final File lockFile = this.getLockFile();
		return lockFile.exists() && lockFile.length() > 0;
	}


	/**
	 * @return A list of command which is common between start as fork and shutdown (script, port, dbpath)
	 */
	private List<String> initCommandsList() {
		final List<String> commands = new LinkedList<>();
		commands.add(this.getScript());
		if (this.getPort() != 0) {
			commands.add("--port");
			commands.add(String.valueOf(this.getPort()));
		}
		if (this.dbpath != null) {
			final File dbDir = new File(this.getDbpath());
			if (!dbDir.exists() && !dbDir.mkdirs()) {
				this.logger.severe("dbPath " + dbDir.getAbsolutePath() + " does not exists or cannot be created... Unable to start " + this.getName() + ".");
				return null;
			}
			if (!dbDir.isDirectory()) {
				this.logger.severe("dbPath " + dbDir.getAbsolutePath() + " is not a directory...  Unable to start " + this.getName() + ".");
				return null;
			}
			commands.add("--dbpath");
			commands.add(this.getDbpath());
		}
		return commands;
	}


	@Override
	public void stop() {
		if (this.status() == State.STOPPED) {
			this.logger.info(this.getName() + " already stopped! Doing nothing...");
		} else {
			final List<String> commands = this.initCommandsList();
			commands.add("--shutdown");
			ProcessUtils.runProcess(this.logger, this.getBinDirectory(), this.getEnv(), commands.toArray(new String[] {}));
		}
	}


	@Override
	public int getPid() {
		if (this.isLocked()) {
			// Server has already been started at least one time. Checking if it is really running (file is not deleted in case of dirty stop).
			if (Utils.isPortAvailable(this.logger, this.getPort())) {
				this.logger.severe("Port " + this.getPort() + " is available but lock file " + this.getLockFile() + " exists. Strange!");
				return -1;
			}
			final File lockFile = this.getLockFile();
			final int pid;
			try {
				pid = Integer.valueOf(FileUtils.readFileToString(lockFile).trim()).intValue();
			} catch (final Exception e) {
				final String msg = "Unable to get pid from file " + lockFile + ".";
				this.logger.severe(msg);
				this.logger.log(Level.FINER, msg, e);
				return -1;
			}
			return pid;
		}
		return -1;
	}


	private File getLockFile() {
		final File lockFile;
		if (this.dbpath == null) {
			lockFile = new File(Mongo.DEFAULT_DB_PATH, Mongo.MONGOD_LOCK);
		} else {
			lockFile = new File(this.dbpath, Mongo.MONGOD_LOCK);
		}
		return lockFile;
	}


	@Override
	public String supportedCommands() {
		return this.getName() + " [start|stop|restart|status] \t will start, stop, restart or retrieve status information about server " + this.getName() + ".\n";
	}


	/**
	 * @return the dbpath
	 */
	public String getDbpath() {
		return this.dbpath;
	}


	/**
	 * @param dbpath
	 *            the dbpath to set
	 */
	public void setDbpath(final String dbpath) {
		this.dbpath = dbpath;
	}


	/**
	 * @return the logpath
	 */
	public String getLogpath() {
		return this.logpath;
	}


	/**
	 * @param logpath
	 *            the logpath to set
	 */
	public void setLogpath(final String logpath) {
		this.logpath = logpath;
	}

}
