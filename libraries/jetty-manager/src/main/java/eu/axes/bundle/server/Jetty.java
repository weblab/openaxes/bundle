/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2014 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package eu.axes.bundle.server;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import javax.management.MBeanServerConnection;

import org.apache.commons.io.FileUtils;
import org.ow2.weblab.bundle.server.WebLabServer;
import org.ow2.weblab.bundle.utils.ProcessUtils;
import org.ow2.weblab.bundle.utils.Utils;

/**
 * An specific implementation of WebLab server to be used in order to manager a new server into the main script.
 *
 * @date 2014-10-14
 * @author ymombrun
 */
public class Jetty extends WebLabServer {


	private static final String JETTY_PID = "jetty.pid";


	private static Set<String> ALLOWED_COMMANDS = new HashSet<>(Arrays.asList("start", "stop", "check", "status", "restart"));


	/**
	 * Just an empty constructor that says that the manager has been created.
	 */
	public Jetty() {
		super();
		this.logger.finest("Jetty Manager started.");
	}


	@Override
	public String getProcessIdentificationClue() {
		this.logger.finest("getProcessIdentificationClue is not implemented by Jetty Manager.");
		return this.getHome();
	}


	@Override
	public boolean isServerFullyStarted(final boolean showDetails) {
		if (!this.isEnabled()) {
			this.logger.info("Can not check if " + this.getName() + " is started since control is disabled.");
			return true;
		}
		this.logger.info("Checking " + this.getName() + " status... ");
		return (this.status() == State.STARTED) && !Utils.isPortAvailable(this.logger, this.getPort());
	}


	/**
	 * Execute a command.
	 *
	 * @param command
	 *            The command to be executed
	 * @return And instance of process
	 */
	public Process execute(final String command) {
		if (!this.isEnabled()) {
			this.logger.info(this.getName() + " discarded command " + command + " since control is disabled.");
			return ProcessUtils.dummyProccess();
		}
		if (Jetty.ALLOWED_COMMANDS.contains(command)) {
			this.logger.warning("Command " + command + " is not handled by " + this.getName() + ".");
			return null;
		}
		if (!this.checkScript()) {
			return null;
		}
		if ("start".equals(command)) {
			return this.start();
		}
		if ("status".equals(command)) {
			this.status();
			return ProcessUtils.dummyProccess();
		}
		if ("stop".equals(command)) {
			this.stop();
			return ProcessUtils.dummyProccess();
		}
		if ("restart".equals(command)) {
			this.stop();
			return this.start();
		}
		return this.execute(this.getEnv(), command);
	}


	@Override
	public MBeanServerConnection getMBeanServerConnection() {
		this.logger.fine("MBeanServerConnection is not handled for server " + this.getName() + ".");
		return null;
	}


	@Override
	public Process start() {
		if (this.status() != State.STOPPED) {
			this.logger.severe(this.getName() + " is not stopped! Aborting start...");
			return null;
		}

		if (ProcessUtils.checkPidFile(this.logger, this, false)) {
			this.logger.severe(this.getName() + " already started! Aborting...");
			return null;
		}

		if (this.isLocked()) {
			this.logger.info(this.getName() + " has not been stopped correctly. Removing lock file.");
			final File lockFile = this.getLockFile();
			if (!lockFile.delete()) {
				this.logger.severe("Unable to remove lock file " + lockFile + ". Please try to remove it manually. Aborting...");
				return null;
			}

		}

		this.logger.info(this.getName() + " is starting...");
		final Process p = this.execute(this.getEnv(), "start");
		final int exitCode;
		try {
			exitCode = p.waitFor();
		} catch (final InterruptedException ie) {
			this.logger.info(this.getName() + " start failed.");
			return null;
		}
		if ((exitCode == 0) && (this.status() == State.STARTED)) {
			this.logger.info(this.getName() + " is started.");
		} else {
			this.logger.info(this.getName() + " start failed.");
			return null;
		}
		return p;
	}


	@Override
	public void stop() {
		this.logger.info(this.getName() + " is stopping...");
		this.execute(this.getEnv(), "stop");
		this.logger.info(this.getName() + " is stopped.");
	}


	@Override
	public int getPid() {
		if (this.isLocked()) {
			// Server has already been started at least one time. Checking if it is really running (file is not deleted in case of dirty stop).
			if (Utils.isPortAvailable(this.logger, this.getPort())) {
				this.logger.severe("Port " + this.getPort() + " is available but lock file " + this.getLockFile() + " exists. Strange!");
				return -1;
			}
			final File pidFile = this.getLockFile();
			try {
				return Integer.valueOf(FileUtils.readFileToString(pidFile).trim()).intValue();
			} catch (final Exception e) {
				final String msg = "Unable to get pid from file " + pidFile + ".";
				this.logger.severe(msg);
				this.logger.log(Level.FINE, msg, e);
			}
		}
		return -1;
	}


	@Override
	public String supportedCommands() {
		return this.getName() + " [start|stop|restart|status] \t will start, stop, restart or retrieve status information about server " + this.getName() + ".\n";
	}


	private File getLockFile() {
		return new File(this.getHome(), Jetty.JETTY_PID);
	}


	private boolean isLocked() {
		final File lockFile = this.getLockFile();
		return lockFile.exists() && (lockFile.length() > 0);
	}


	/**
	 * @return Return <code>true</code> if bin directory and script exists and can be executed.
	 */
	private boolean checkScript() {
		final File binDir = new File(this.getBinDirectory());
		if (!binDir.exists()) {
			this.logger.severe("binDirectory " + binDir.getAbsolutePath() + " does not exists... Unable to run start command inside.");
			return false;
		}
		if (!binDir.isDirectory()) {
			this.logger.severe("binDirectory " + binDir.getAbsolutePath() + " is not a directory... Unable to run start command inside.");
			return false;
		}
		final File script = new File(binDir, this.getScript());
		if (!script.exists()) {
			this.logger.severe("script " + script.getAbsolutePath() + " does not exists... Unable to run it to start server.");
			return false;
		}
		if (!script.isFile()) {
			this.logger.severe("script " + script.getAbsolutePath() + " is not a file... Unable to run it to start server.");
			return false;
		}
		if (!script.canExecute()) {
			this.logger.severe("script " + script.getAbsolutePath() + " is not executable... Unable to run it to start server.");
			return false;
		}
		return true;
	}

}
