package eu.axes.engine.camel;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * This simple class contains a few utility methods that could be called from the chain.
 * 
 * @author ymombrun
 * @date 2015-01-11
 */
public class AxesUtility {


	private static final String CLASSIFIER_LOCATION = "classifier.location";


	private static final String ON_THE_FLY_CLASSIFIER_LOCATION = "otf.classifier.location";


	private final Log log;


	private final boolean classifierInstalled;


	private final boolean onTheFlyClassifierInstalled;



	/**
	 * The default constructor of the class.
	 */
	public AxesUtility() {
		this.log = LogFactory.getLog(this.getClass());

		this.classifierInstalled = anythingAtLocationOfProperty(CLASSIFIER_LOCATION);
		this.onTheFlyClassifierInstalled = this.anythingAtLocationOfProperty(ON_THE_FLY_CLASSIFIER_LOCATION);
	}


	/**
	 * @param locationProperty
	 *            The system property that should contain the location of the folder to check
	 * @return <code>true</code> if and only if the property refers to a non empty existing directory
	 */
	boolean anythingAtLocationOfProperty(String locationProperty) {
		final String location = System.getProperty(locationProperty);
		boolean installed;
		if (location == null) {
			this.log.warn("Property " + locationProperty + " is not found. Considering as not installed.");
			installed = false;
		} else {
			final File file = new File(location);
			if (file.exists() && file.isDirectory() && file.listFiles().length > 0) {
				installed = true;
			} else {
				installed = false;
			}
		}
		return installed;
	}


	/**
	 * @return <code>true</code> if the folder provided in the environment variable classifier.location exists and is not empty. <code>false</code> in any other case (including exceptions).
	 */
	public boolean isClassifierInstalled() {
		return this.classifierInstalled;
	}


	/**
	 * @return <code>true</code> if the folder provided in the environment variable otf.classifier.location exists and is not empty. <code>false</code> in any other case (including exceptions).
	 */
	public boolean isOnTheFlyClassifierInstalled() {
		return this.onTheFlyClassifierInstalled;
	}


}
