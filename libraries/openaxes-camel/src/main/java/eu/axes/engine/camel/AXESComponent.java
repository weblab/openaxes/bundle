package eu.axes.engine.camel;

import java.util.Map;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.impl.DefaultComponent;

/**
 * Represents the component that manages the instance of {@link AXESEndpoint}.
 */
public class AXESComponent extends DefaultComponent {


	/**
	 * Default constructor
	 */
	public AXESComponent() {
		super();
	}


	/**
	 * Constructor sending camel context to the parent
	 * 
	 * @param context
	 *            The camel context to be used
	 */
	public AXESComponent(final CamelContext context) {
		super(context);
	}



	@Override
	protected Endpoint createEndpoint(final String uri, final String remaining, final Map<String, Object> parameters) throws Exception {
		final Endpoint endpoint = new AXESEndpoint(uri, this);
		this.setProperties(endpoint, parameters);
		return endpoint;
	}

}
