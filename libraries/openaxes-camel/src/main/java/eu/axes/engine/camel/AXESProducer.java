package eu.axes.engine.camel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.text.Normalizer;
import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.NoTypeConversionAvailableException;
import org.apache.camel.TypeConverter;
import org.apache.camel.impl.DefaultProducer;
import org.apache.camel.spi.TypeConverterRegistry;
import org.apache.camel.util.MessageHelper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Video;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.purl.dc.terms.DCTermsAnnotator;

import eu.axes.utils.AxesAnnotator;

/**
 * The WebLab producer.
 *
 * To perform actions on WebLab resources:
 * - create a new resource
 *
 * @author ymombrun, based on emilienbondu's WebLabProducer
 */
public class AXESProducer extends DefaultProducer {


	private final AXESEndpoint endpoint;


	/**
	 * @param endpoint
	 *            The endpoint used inside
	 */
	public AXESProducer(final AXESEndpoint endpoint) {
		super(endpoint);
		this.endpoint = endpoint;
	}


	@Override
	public void process(final Exchange exchange) {
		MessageHelper.copyHeaders(exchange.getIn(), exchange.getOut(), true);

		final Object languageOptionalHeader = exchange.getIn().getHeader("weblab:dc:language");
		final String language;
		if (languageOptionalHeader == null) {
			language = null;
		} else {
			language = languageOptionalHeader.toString();
		}

		final URI endpointURI = URI.create(this.endpoint.getEndpointUri());
		if ((endpointURI.getAuthority() != null) && endpointURI.getAuthority().equalsIgnoreCase("create")) {
			try {
				this.create(exchange, language);
			} catch (final WebLabCheckedException wlce) {
				throw new WebLabUncheckedException(wlce);
			}
		} else {
			final String message = "AXESEndpoint does not support this kind of URI: " + endpointURI.toString();
			this.log.error(message);
			throw new UnsupportedOperationException(message);
		}
	}


	private void create(final Exchange exchange, final String language) throws WebLabCheckedException {
		final File file = this.tryConvertTo(File.class, exchange);
		final String path = FilenameUtils.normalize(file.getAbsolutePath());

		this.log.debug("Create AXES resource from file " + path + ".");

		final String originalFilename = file.getName();
		final String videoId = this.createVideoId(FilenameUtils.getBaseName(originalFilename));

		final long size = file.length();
		if (size == 0) {
			final String msg = "This file is empty (" + path + ").";
			this.log.error(msg);
			throw new WebLabUncheckedException(msg);
		}

		final String humanReadableSize = FileUtils.byteCountToDisplaySize(size);
		final Date lastModified = new Date(file.lastModified());

		/*
		 * Create the document and annotate it. It uses an uri scheme which is not the same than the original weblab one. Moreover, it contains specific annotations.
		 */
		final Document document = new Document();
		document.setUri("http://axes-project.eu/" + this.endpoint.getCollectionId() + "/" + videoId);
		final Annotation documentAnnot = WebLabResourceFactory.createAndLinkAnnotation(document);
		final DublinCoreAnnotator docDCA = this.annotateWithCommonProperties(document, documentAnnot, file, videoId);
		docDCA.writeSource(path);
		if (language != null) {
			docDCA.writeLanguage(language);
		}

		final WProcessingAnnotator docWPA = new WProcessingAnnotator(URI.create(document.getUri()), documentAnnot);
		docWPA.writeOriginalFileName(originalFilename);
		docWPA.writeOriginalFileSize(Long.valueOf(size));
		final DCTermsAnnotator dcta = new DCTermsAnnotator(URI.create(document.getUri()), documentAnnot);
		dcta.writeExtent(humanReadableSize);
		dcta.writeModified(lastModified);

		/*
		 * Create the video and annotate it. It uses an uri scheme which is not the same than the original weblab one. Moreover, it contains specific annotations.
		 */
		final Video video = new Video();
		video.setUri(document.getUri() + "#mainVideo");
		document.getMediaUnit().add(video);
		final Annotation videoAnnot = WebLabResourceFactory.createAndLinkAnnotation(video);
		this.annotateWithCommonProperties(video, videoAnnot, file, videoId);

		exchange.getOut().setHeader("weblabURI", document.getUri());

		final StringWriter writer = new StringWriter();
		this.endpoint.getMarshaller().marshalResource(document, writer);
		exchange.getOut().setBody(writer);

		this.log.trace("Resource outputing done.");
	}


	private String createVideoId(final String baseName) {
		// Decompose unicode String by separating accents from their letters (needed to fix OPENAXES-81)
		final char[] videoIdChars = Normalizer.normalize(baseName, Normalizer.Form.NFKD).toCharArray();
		// Replace every non letter/digit/space chars by a space
		for (int i = 0; i < videoIdChars.length; i++) {
			final char c = videoIdChars[i];
			if (!Character.isLetterOrDigit(c) && (c != ' ')) {
				videoIdChars[i] = ' ';
			}
		}

		// Remove start and and spaces. Do not let more than one consecutive space and finally replace space by undescore.
		return 'v' + new String(videoIdChars).trim().replaceAll("\\s+", " ").replace(' ', '_');
	}



	/**
	 * @param file
	 *            The source file to be stored as native content and from which we shall extract the mime type from the extension.
	 * @param resource
	 *            The resource to be annotated
	 * @param annotation
	 *            The annotation to be enriched
	 * @param videoId
	 *            The video Id
	 * @return The created {@link DublinCoreAnnotator} instance of the resource.
	 * @throws WebLabCheckedException
	 */
	private DublinCoreAnnotator annotateWithCommonProperties(final Resource resource, final Annotation annotation, final File file, final String videoId) throws WebLabCheckedException {
		final AxesAnnotator aa = new AxesAnnotator(URI.create(resource.getUri()), annotation);
		aa.writeCollectionId(this.endpoint.getCollectionId());
		aa.writeVideoId(videoId);

		final String extension = FilenameUtils.getExtension(file.getName());
		final String format = this.endpoint.getMimeMap().get(extension);
		if (format == null) {
			final String msg = "This file extension of file " + file.getAbsolutePath() + " is not handled.";
			this.log.error(msg);
			throw new WebLabUncheckedException(msg);
		}

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(URI.create(resource.getUri()), annotation);
		dca.writeFormat(format);

		final URI contentUri;
		try (final FileInputStream fis = new FileInputStream(file)) {
			contentUri = this.endpoint.getContentWriter().writeContent(fis, resource, extension);
		} catch (final IOException ioe) {
			final String message = "Unable to write content for resource " + resource.getUri() + " and extension " + extension + ".";
			this.log.error(message);
			throw new WebLabCheckedException(message, ioe);
		}

		new WProcessingAnnotator(URI.create(resource.getUri()), annotation).writeNativeContent(contentUri);

		dca.startInnerAnnotatorOn(contentUri);
		dca.writeFormat(format);
		dca.endInnerAnnotator();

		return dca;
	}


	/**
	 * Tries to convert the body of the provided exchange object to the specified type in the context of that exchange, throwing an exception if not possible to convert.
	 *
	 * @param toClass
	 *            the requested type
	 * @param exchange
	 *            the current exchange
	 * @return the converted value or <code>null</code> if the value is <code>null</code>.
	 */
	private <T> T tryConvertTo(final Class<T> toClass, final Exchange exchange) {
		final Object body = exchange.getIn().getBody();

		if (body == null) {
			this.log.debug("Body was null. Nothing to convert.");
			return null;
		}

		// check if value is already in a compatible class
		if (toClass.isInstance(body)) {
			this.log.debug("Body is already an instance of " + toClass.getName() + ". Nothing to convert.");
			return toClass.cast(body);
		}

		// else we try to convert the value
		final TypeConverterRegistry tcRegistry = exchange.getContext().getTypeConverterRegistry();
		final TypeConverter typeConvert = tcRegistry.lookup(toClass, body.getClass());
		if (typeConvert == null) {
			final String message = "No type convert found to convert the body " + body + " into an instance of " + toClass.getName() + ".";
			this.log.error(message);
			throw new WebLabUncheckedException(message, new NoTypeConversionAvailableException(body, toClass));
		}
		return typeConvert.tryConvertTo(toClass, exchange, body);
	}

}