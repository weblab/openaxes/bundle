package eu.axes.engine.camel;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Consumer;
import org.apache.camel.Processor;
import org.apache.camel.Producer;
import org.apache.camel.impl.DefaultConsumer;
import org.apache.camel.impl.DefaultEndpoint;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;

import eu.axes.components.contentmanager.AxesContentWriter;

/**
 * Represents the AXES endpoint. Inspired from the WebLabEndpoint
 *
 * @author ymombrun
 *
 */
public class AXESEndpoint extends DefaultEndpoint {


	private final Map<String, String> mimeMap;


	private String collectionId;


	private final WebLabMarshaller marshaller;


	private final AxesContentWriter contentWriter;


	/**
	 * @param uri
	 *            The uri of the endpoint
	 * @param component
	 *            The component that has created that endpoint
	 */
	public AXESEndpoint(final String uri, final AXESComponent component) {
		super(uri, component);
		this.mimeMap = new HashMap<>();
		this.mimeMap.put("mpg", "video/mpeg");
		this.mimeMap.put("mpeg", "video/mpeg");
		this.mimeMap.put("ts", "video/mp2t");
		this.mimeMap.put("avi", "video/avi");
		this.mimeMap.put("webm", "video/webm");
		this.mimeMap.put("flv", "video/x-flv");
		this.mimeMap.put("wmv", "video/x-ms-wmv");
		this.mimeMap.put("mov", "video/quicktime");
		this.mimeMap.put("ogv", "video/ogg");
		this.mimeMap.put("mp4", "video/mp4");
		this.mimeMap.put("m4v", "video/mp4");
		this.collectionId = "cOpenAXES";
		this.marshaller = new WebLabMarshaller(true);
		this.contentWriter = new AxesContentWriter();
	}


	@Override
	public Producer createProducer() throws Exception {
		return new AXESProducer(this);
	}


	@Override
	public Consumer createConsumer(Processor processor) throws Exception {
		return new DefaultConsumer(this, processor);
	}


	@Override
	public boolean isSingleton() {
		return true;
	}


	/**
	 * @return the mimeMap
	 */
	public Map<String, String> getMimeMap() {
		return this.mimeMap;
	}


	/**
	 * @param mimeMap
	 *            the mimeMap to add to exiting mimeMap
	 */
	public void setMimeMap(final Map<String, String> mimeMap) {
		this.mimeMap.putAll(mimeMap);
	}


	/**
	 * @return the collectionId
	 */
	public String getCollectionId() {
		return this.collectionId;
	}


	/**
	 * @param collectionId
	 *            the collectionId to set (a 'c' at the start will be added if not already present)
	 */
	public void setCollectionId(final String collectionId) {
		if (collectionId == null) {
			throw new IllegalArgumentException("The provided collection id is null but cannot.");
		}
		if (collectionId.trim().isEmpty()) {
			throw new IllegalArgumentException("The provided collection id is empty but cannot.");
		}
		if (collectionId.trim().startsWith("c")) {
			this.collectionId = collectionId;
		} else {
			this.collectionId = 'c' + collectionId;
		}

	}


	/**
	 * @return the marshaller
	 */
	public WebLabMarshaller getMarshaller() {
		return this.marshaller;
	}


	/**
	 * @return the contentManager
	 */
	public AxesContentWriter getContentWriter() {
		return this.contentWriter;
	}

}
