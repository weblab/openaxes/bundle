#!/bin/sh
#
# OpenAXES manager Script
#

DIRNAME=`dirname "$0"`

# Set the path to the java executable
JAVA="$JAVA_HOME/bin/java"
[ -x "$JAVA" ] || JAVA="`which java`" || {
        echo "ERROR: java executable not found" >&2
        exit 1
}

# Set the path to weblab bootstrap jar
WEBLAB_JARS="$DIRNAME/weblab-launcher.jar:$DIRNAME/data/lib/*"

# Original command was exec .... but when using exec chmod are not called after.
$JAVA -cp "$WEBLAB_JARS" org.ow2.weblab.bundle.Launcher "$@"

# Dirty fix for OPENAXES-80. Once issues WEBLAB-1229 and WEBLAB-1236 will be solved this could be removed
chmod 777 -Rf data/content-manager/
chmod 777 -Rf data/gui/
chmod 777 -Rf axes-research-0.8.1/
chmod 777 -Rf data/mongo 
