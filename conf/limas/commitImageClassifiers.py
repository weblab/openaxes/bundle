# -*- encoding: utf-8 -*-
# coding=utf-8
from __future__ import with_statement
from optparse import OptionParser
import sys
import re
import os 
import logging
import glob
import itertools
import csv
logging.basicConfig(level=logging.DEBUG)
import platform
if platform.system() == "Java":
   import com.xhaus.jyson.JysonCodec as json
   sys.add_package("eu.axes.limas.core.elements")
   sys.add_package("eu.axes.limas.core.util")
   sys.add_package("eu.axes.limas.core.impl.util")
   sys.add_package("eu.axes.limas.core")
   from eu.axes.limas.core.elements import *
   from eu.axes.limas.core.util import *
   from eu.axes.limas.core.impl.util import *
   from eu.axes.limas.core import *
   from eu.axes.limas.core.util import *
else:
   import json

def create_information_manager(configfile): 
   '''
   Create an information manager with the given Jython configuration file
   '''
   from java.io import File
   from eu.axes.limas.core import InformationManagerFactory
   from eu.axes.limas.core import InformationManager
   from os.path import abspath
   factory = InformationManagerFactory()
   file = File(abspath(configfile))
   manager = factory.createInformationManager(file)
   return manager

def loadNames(modelNames):
   with open(modelNames,'rb') as tsvin:
      tsvReader = csv.reader(tsvin, delimiter='\t')
      modelMap = dict()
      for row in tsvReader:
         modelMap.update({row[0]:row[1]})
      
   return modelMap

def preprocessUri(uri):
   extension = re.compile('\.[^/]+$')
   endNumber = re.compile('^(.+?)(\d+)$')
   uri = extension.sub('', uri)
   if uri.startswith('//'):
      uri = uri[1:]
   if opt.upshot:
      m = endNumber.match(uri)
      if m:
         l = len(m.group(2))
         f = "%s%0"+str(l)+"d"
         uri = f % (m.group(1), int(m.group(2))+1)
      else:
         print 'no match'  
   return uri
  
def getShot(uri):
   try:
      i = uri.find('/s')
      z = uri.find('/', i+1)
      if z < 0: z = len(uri)
      return uri[:z]
   except:
      return uri
    
def getVideo(uri):
   try:
      i = uri.find('/v')
      z = uri.find('/', i+1)
      if z < 0: z = len(uri)
      return uri[:z]
   except Exception,e:
      return uri

def index(manager, dataset, classifier, resp):
   import com.xhaus.jyson.JysonCodec as json
   from java.lang import Float
   from java.net import URI
   from eu.axes.limas.core.util import RankNormalizer
   from eu.axes.limas.core.util import StatValue
   from eu.axes.limas.core.elements import Entity
   from eu.axes.limas.core.elements import Segment
   from eu.axes.limas.core.impl.util import VectorRanking

   threshold = resp.get('threshold', None)
   topn = opt.TOP
   if topn: topn = int(topn)

   normalizer = RankNormalizer()
   if opt.topn:
      opt.topn = int(opt.topn)
      resp['ranking'] = resp['ranking'][:opt.topn]

   # Get entity instance  
   e = None
   if 'wnid' in resp:
      print resp['wnid'] + " " + classifier
      e = manager.getEntityByExternalID(resp['wnid'])

   if not e:
      e = manager.createEntity(Entity.Type.UEBERCLASSIFIER, classifier)
      if 'wnid' in resp:
   	     e.addExternalID(resp['wnid'])
  
   eid = e.getEID().id        
   print 'Classifier %s (limasId %d)' % (classifier, eid)
   if (e.getOccurrenceRanking() != None) and opt.overwriteExisting == "False": 
      print "   Ignoring entity: already indexed"
      return 
  
   # Set labels
   e.put(Entity.ENTITY_NAME, classifier.encode('utf-8'))
   e.put(Entity.ENTITY_NAME + "_en", classifier)
   e.englishLabels = [classifier]
   e.nativeLabels = [classifier]

   # go through ranking and generate a limas conform ranking from it
   idRanking = VectorRanking()
   minScore = 1 # keep track of minimum score, if classifier definition doesn't carry score statistics
   occ = StatValue(False)
   ranking = []
   for rank,res in enumerate(resp['ranking']):
      uri = preprocessUri(res['id'])
      s = manager.getSegmentByURI(URI.create(uri))
      score = float(res['score'])
      if (threshold != None and score > threshold) or (topn != None and rank < topn):
         ranking.append((getShot(uri), score))
      if score < minScore:
         minScore = score
      occ.add(minScore, 1)
      if s != None:
         id = s.getID()
         idRanking.add(id, score)
      else:
         print uri, "not found"
   if opt.normalize:
      normalizer.normalize(idRanking)
   e.setOccurrenceRanking(idRanking)
    
   # optionally go through the ranking and add the occurrences to the segments
   segs = []  
   if threshold or topn:
      print "do labeling of ", len(ranking)
      ranking.sort()
      for video, shots in itertools.groupby(ranking, key=lambda v: getVideo(v[0])):
         videoSeg = manager.getSegmentByURI(URI.create(video))
         videoFV = videoSeg.getOccurrences(Entity.Type.UEBERCLASSIFIER)
         if videoFV == None:
            videoFV = FloatFeatureVector()
         videoSeg.setOccurrences(Entity.Type.UEBERCLASSIFIER, videoFV)
         c = 0
         for shot, score in shots:
            segment = manager.getSegmentByURI(URI.create(shot))
            if not segment: 
        	   continue
            fv = segment.getOccurrences(Entity.Type.UEBERCLASSIFIER)
            if fv == None:
               fv = FloatFeatureVector()
            segment.setOccurrences(Entity.Type.UEBERCLASSIFIER, fv)
            fv.set(eid, Float(1))
            c += 1        
            segs.append(segment)
         videoFV.set(eid, Float(c))
         segs.append(videoSeg)

   if 'meanscore' in resp:
      e.put('meanScore', str(resp['meanscore']))
   else:
      e.put('meanScore', minScore / 2.0)
   if 'stdscore' in resp:
      e.put('stdScore', str(resp['stdscore']))
   else:
      e.put('stdScore', str(0.0))
   e.setOccurrences(Segment.Type.VIDEO, occ)
   e.put(Segment.UEBERCLASSIFIER, '1')
   if opt.marker:
      e.put("detectorType", opt.marker)
   if len(segs) > 0: manager.addSegments(segs)
   manager.addEntity(e)
  

if __name__ == "__main__":
   parser = OptionParser(usage="usage: %prog [options] ")
   parser.add_option("-d", "--dataset", dest="dataset", help="dataset to download", metavar="dataset", default="cOpenAXES")
   parser.add_option("-c", "--classifiers", dest="classifiers", help="suffix to append", metavar="classifiers", default="*")
   parser.add_option("-o", "--overwriteExisting", dest="overwriteExisting", help="overwrite existing occurrences", metavar="overwriteExisting", default=True)
   parser.add_option("-m", "--manager", dest="manager", help="config file", metavar="manager", default=None)
   parser.add_option("-n", "--normalize", dest="normalize", help="do normalization", metavar="normalize", default=False)
   parser.add_option("-M", "--mark", dest="marker", help="marker", metavar="marker", default="leuven")
   parser.add_option("-S", "--up", dest="upshot", help="upshot", metavar="upshot", default=None)
   parser.add_option("-t", "--topn", dest="topn", help="only consider the topn", metavar="topn", default=None)
   parser.add_option("-T", "--TOPTRUE", dest="TOP", help="Number of top ranked shots to treat as relevant", metavar="TOP", default=None)
   parser.add_option("-N", "--names", dest="modelNames", help="Path to TSV file with a wnid and an english name per line", metavar="modelNames", default=None)
   parser.add_option("-f", "--file", dest="file", help="The path to folder to load", metavar="file", default=None)
   (opt, args) = parser.parse_args()
   if len(args) > 1:
      parser.error('Wrong number of arguments given (received %s, parsed to %s)' % (args, opt))

   if not opt.manager:
      parser.error('Must specify a path to a manager configuration file using -m')
   if not opt.modelNames:
      parser.error('Must specify a path to model names file using -N')
   if not opt.file:
      parser.error('Must specify a path to a folder containing classifiers using -f')

   if opt.classifiers == '*':
      def extract(f):
         m = re.match(r'.*?-(.*)\.json', f)
         return m.group(1)
      classifiers = [ extract(f) for f in glob.glob('%s-*.json' % opt.dataset) ]
   else:
      classifiers = [ opt.classifiers ]


   manager = create_information_manager(opt.manager) 
   modelMap = loadNames(opt.modelNames)
   inFile = opt.file
   for fn in os.listdir(inFile):
      if not (fn.endswith('.json') or fn.endswith('.txt')): continue
      print 'loading', fn
      f = open(os.path.join(inFile, fn))
      resp = json.loads(f.read())
      f.close()
      if 'names' not in resp:
          resp['names'] = modelMap[resp['wnid']]

      index(manager, opt.dataset, resp['names'], resp)           
