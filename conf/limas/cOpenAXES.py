#
# This script is intended to be called from inside InformationManagerFactory to configure instances of the information manager.
# This is not intended to be called directly!
#

import os
from eu.axes.limas.core.elements import Segment

#
# Constants
#
DB_HOST				= 'localhost'
DB_PORT				= 27017
COLLECTION			= 'cOpenAXES'
# Get parent if current dir is jetty ones (dirty but jetty and tomcat are not resolving current directory the same way and I have no access to Java environment variables).
WEBLAB_HOME			= os.path.realpath(os.getcwd())
if os.path.basename(WEBLAB_HOME) == 'jetty-distribution-9.2.3.v20140905':
    WEBLAB_HOME		= os.path.dirname(WEBLAB_HOME)

LIMAS_DATA_DIR		= os.path.join(os.path.join(WEBLAB_HOME, 'data'), 'limas')
COLLECTIONS_PATH	= os.path.join(LIMAS_DATA_DIR, 'collections')
INDICES_PATH		= os.path.join(LIMAS_DATA_DIR, 'indices')
LANGUAGE			= 'en'
LUCENE_INDICES_PATH	= os.path.join(os.path.join(INDICES_PATH, 'lucene'), COLLECTION)
VISOR_INDEX_PATH	= os.path.join(os.path.join(INDICES_PATH, 'category-t'), COLLECTION)
IMAGE_GETTER_PORT	= 35200
VISOR_PORT 			= 35217
URL_PREFIX			= 'http://localhost/'
DB_PREFIX			= COLLECTION + '_'


# Shot level search fields                                                                                                                                   
ASR_FIELDS =  [
    Segment.ASR,		None,
    Segment.ENTITIES,	None,
]


# Video level search fields                   
METADATA_FIELDS = [
    Segment.TITLE,				'title',
    Segment.KEYWORDS,			'keywords',
    Segment.SUMMARY,			'summary',
    Segment.SUBJECT,			None,
    Segment.UPLOADER,			None,
    Segment.PERSONS,			'persons',
    Segment.OBJECTS,			'objects',
    Segment.PLACES,				'places',
    Segment.ENTITIES,			'entities',
    Segment.PUBLICATIONDATE,	'date'
]


# Simple mkDirs method
def mkDirs(path):
    import os
    try: 
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise


# Create needed directories
mkDirs(LUCENE_INDICES_PATH)


def configImageGetter(config):
    config.imageGetterPort= IMAGE_GETTER_PORT
    config.imageBasePath  = os.path.join(os.path.join(COLLECTIONS_PATH, COLLECTION), 'imagegetter')
    config.urlPrefix      = URL_PREFIX + '/imagegetter'
    mkDirs(config.imageBasePath)


def configCategoryT(ksf):
    mkDirs(os.path.join(VISOR_INDEX_PATH, 'classifiers'))
    mkDirs(os.path.join(VISOR_INDEX_PATH, 'annotations'))
    mkDirs(os.path.join(VISOR_INDEX_PATH, 'features'))
    ksf.config.datasetName   = COLLECTION
    ksf.config.basePath      = VISOR_INDEX_PATH
    ksf.config.retrieveShots = False
    ksf.config.resultsToRetrieve = 1000
    ksf.config.setRewritePattern('/[^/]+$')
    ksf.config.setRewriteRule('')
    ksf.config.newPathRewriter()
    #ksf.config.setDebug(False)
    ksf.config.pathRewriter.add("^(.*)$", "/" + COLLECTION + "$1")
    configImageGetter(ksf.config)
    return


from eu.axes.limas.core.impl    import Normalizer
Normalizer.createNormalizer(LANGUAGE)


'''
(source, impl, id, url, database, humanReadable, searchURL)
source in {
  #meta, search in metadata
  #speech search in speech
  #category-t search for categories identified by text 
  #entity search for pre-stored entities
  #instance-i search for instances identified by example images 
}

implementation in {
  LUCENE: local lucene database
  MONGODB_ENTITY: ranked list in local mongodb 
  SOLR: remote image search 
  CpuVisor: new cpu visor interface
}
'''
services = [
    ('#meta',		'LUCENE',			'Meta',		COLLECTION,	'%s in metadata',			os.path.join(LUCENE_INDICES_PATH, 'meta_lucene.idx'),						METADATA_FIELDS),
	('#speech',		'LUCENE',			'Speech',	COLLECTION,	'%s in speech',				os.path.join(LUCENE_INDICES_PATH, 'asr_lucene.idx'),						ASR_FIELDS),
	('#category-t',	'CpuVisor',			'OXUCat',	COLLECTION,	"broad term '%s'",			'127.0.0.1:' + str(VISOR_PORT),															configCategoryT),
	('#entity',		'MONGODB_ENTITY',	'Entity',	COLLECTION,	'%s in video content',		'',																			None),
	('#instance-i', 'SOLR',      		'INRIAIns', COLLECTION, 'near duplicate images',	'http://localhost:8181/pastec-indexer/pastec/search/' + COLLECTION + '/%s', None)
]



import types 
import sys

def instantiate(manager, factory, DB_HOST, DB_PORT, COLLECTION, INDEX_PATH, URL_PREFIX, ASR_FIELDS, METADATA_FIELDS, services):
    # ==========================================================
    # General Instance Configuration
    #
    from eu.axes.limas.core.impl.mongodb    import MongoDBConnection
    hconf = MongoDBConnection(DB_HOST, DB_PORT, COLLECTION)
    hconf.register(manager)

    from eu.axes.limas.core.impl.entityclassifier    import LingPipeClassifier
    from eu.axes.limas.core.impl    import Normalizer
    manager.queryParser = LingPipeClassifier(manager.entityStore)
    manager.addEntityRecommender(manager.queryParser)

    # Instantiate URL handler (projecting relative urls to absolute ones) 
    from eu.axes.limas.core.impl    import URIHandlerStandard
    uriHandler = URIHandlerStandard(URL_PREFIX)
    manager.setUriHandler(uriHandler)

    # ==========================================================
    # Indexing Components (LUCENE only in fact)
    #
    for service, impl, idS, dataset, humanReadable, searchURL, contentInfo in services:
        if impl == 'LUCENE': 
            mkDirs(searchURL)
            from eu.axes.limas.core.impl.lucene    import LuceneIndexer
            indexer = LuceneIndexer(manager.entityStore, searchURL, contentInfo)
            if contentInfo == ASR_FIELDS:
                indexer.setRequiredSegmentType('s')
            else:
                indexer.setRequiredSegmentType('v')
            manager.addIndexer(indexer)

    # ==========================================================
    # Search Components
    #

    # Add ASR linker
    from eu.axes.limas.core.impl.linking import ASRLinker
    linker = ASRLinker(manager.entityStore, manager)
    manager.setLinker(linker)

    # Instantiate AdvancedFusion searcher
    from eu.axes.limas.core.impl.fusion    import AdvancedFusionSearcherFactory
    factory = AdvancedFusionSearcherFactory(manager.entityStore)
    factory.setRequiredSegmentType('s|v')
    factory.setDebug(False)
    factory.setDoCache(False)
    #factory.setWindowSize(1)
    factory.setRecommender(manager.queryParser)
    manager.addSearcherFactory(factory)

    for service, impl, idS, dataset, humanReadable, searchURL, contentInfo in services:
        if impl == 'LUCENE':
            from eu.axes.limas.core.impl.lucene    import LuceneKSFactory
            ksf = LuceneKSFactory(manager.entityStore, searchURL)
        elif impl == 'ENTITY':
            from eu.axes.limas.core.impl.lucene    import LuceneEntityOccurrences
            leo = LuceneEntityOccurrences(manager.entityStore, searchURL)
            ksf = leo.getKSFactory()
            manager.addIndexer(leo.getIndexer())
            factory.addSubComponent(leo)
        elif impl == 'MONGODB_ENTITY':
            from eu.axes.limas.core.impl.mongodb    import MongoDBEntityKSFactory
            ksf = MongoDBEntityKSFactory(manager.entityStore)
        elif impl == 'BackendAPI' and service.endswith('-t'):
            from eu.axes.limas.core.impl.otf.backend    import BackendKSFactory
            ksf = BackendKSFactory(manager.entityStore)
            ksf.datasetName = dataset
            ksf.resultsToRetrieve = 1000
            host = searchURL[0:searchURL.index(":")]
            port = int(searchURL[searchURL.index(":")+1:])
            ksf.config.setBackendHostName(host)
            ksf.config.setBackendPort(port)
            if not isinstance(contentInfo, types.FunctionType):        
                ksf.config.setBasePath(contentInfo)
        elif impl == 'CpuVisor' and service.endswith('-t'):
            from eu.axes.limas.core.impl.otf.cpuvisor import CpuVisorKSFactory
            ksf = CpuVisorKSFactory(manager.entityStore)
            ksf.datasetName = dataset
            ksf.resultsToRetrieve = 1000 
        elif impl == 'SOLR' and service.endswith('-i'):
            from eu.axes.limas.core.impl.similarity import InriaSimilarityKSFactory
            ksf = InriaSimilarityKSFactory(manager.entityStore)
            ksf.searchURL = searchURL
        else:
            print "Unknown impl typ " + impl
            sys.exit(1)

        if contentInfo != None and isinstance(contentInfo, types.FunctionType):
            contentInfo(ksf)

        ksf.ID = idS
        ksf.humanReadable = humanReadable

        # add to the right knowledgesource
        if   service == "#meta":       factory.addMetaFactory(ksf)
        elif service == "#speech":     factory.addSpeechFactory(ksf)
        elif service == "#entity":     factory.addEntityFactory(ksf)
        elif service == "#category-t": factory.addCategoryTFactory(ksf)
        elif service == "#instance-i": factory.addInstanceIFactory(ksf)
        else: print "error"

        if service != "#entity": factory.addSubComponent(ksf)

instantiate(manager, factory, DB_HOST, DB_PORT, COLLECTION, LUCENE_INDICES_PATH, URL_PREFIX, ASR_FIELDS, METADATA_FIELDS, services)
