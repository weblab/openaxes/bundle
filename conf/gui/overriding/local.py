
#
# Absolute path of where to store media files (uploaded images, etc.). This 
# should be below your web root so that the web server can be used to host
# these files
#
MEDIA_ROOT = '/var/www/html/axes-research/media/'

#
# Base URL for media files (uploaded images, etc.). This URL should correspond
# to the URL for MEDIA_ROOT. 
#
MEDIA_URL = '/axes-research/media/'

#
# Absolute path of where to store static files (css, javascript, etc.). This 
# should be below your web root so that the web server can be used to host
# these files
#
STATIC_ROOT = '/var/www/html/axes-research/static/'

#
# Base URL for static. This URL should correspond to the URL for STATIC_ROOT. 
#
STATIC_URL = '/axes-research/static/'

#
# Where to store the admin interface static files and media.
#
ADMIN_MEDIA_PREFIX = STATIC_URL + 'admin/'

#
# URL of the LIMAS JSON RPC interface to connect to
#
SERVICE_URL = 'http://localhost:8099/json-rpc'

#
# Mongo DB database name
#
DATABASE_NAME = 'axesresearch'

#
# Time zone of the server
#
TIME_ZONE = 'Europe/Paris'

#
# Post processing rules for LIMAS responses. A dictionary mapping
# response keys to a list of regular expression replacement rules to the
# corresponding values. For example:
#
# LIMAS_RESPONSE_POSTPROCESSING_RULES = {
#     'thumbnailUrl': [
#         (r'^(.*)$', r'http://axes.ch.bbc.co.uk/thumbs/thumbnail?image=\1')
#     ]
# }
#
LIMAS_RESPONSE_POSTPROCESSING_RULES = { }

LIMAS_CACHE_ENABLED = False
