This is the Open AXES package for Linux 64 Debian/Ubuntu based.


Open AXES contains software that have already been compiled and that are available only on such a target platform.
Moreover, the installation and compilation script is bound on Ubuntu package delivery system and would have to be changed to work on other Linux distributions.
It has been validated on Ubuntu 14.04, 14.10 and 15.04 as well as on Mint 17.1.


The package is made of components with various licences. You will find more details in the data/licences folder.
To make it short, most of the system is delivered using free and open source non contaminating licences (LGPL, MIT and Apache 2 licences).
Two components are releases with more restrictive licences and without the sources (but are free for research purposes):
	- Technicolor shot detector and key frame extractor
	- KU Leuven image classifier

 

Prerequisites:
	- A supported operating system (with a preference over Ubuntu 14.04 64 bits).
	- A lot of free ports, at least 80, 1099, 4212, 8099, 8181, 8282, 18181, 27017 and 61616.
	- A quite powerful computer with 15GB of free space, at least 4GB of RAM and at least 2 CPUs.
	- A user which is allowed to use the sudo command, some package will have to be installed.
	- An Internet access in order to install the missing packages.



In order to install Open AXES on your system:
 
 
	1)	Unzip the package on your system.

	2)	Open a terminal and launch the installation process by calling the install.sh script.
			> ./install.sh
		It has a few options (listed below) that enables the inclusion of optional functionality (like classifiers, GPL H264 codec for FFMPEG...).
			This script should be run by the normal system user.
			During the installation you will have to confirm several steps, especially during the installation of the JDK, as well as authorise some installation parts as a super user.
			It will install various libraries that are needed like (not exhaustive):
				- apache2
				- python and a lot of python modules
				- an Oracle JDK 7,
				- opencv, openblas, protobuf and a lot of development tools if includeOnTheFlyClassifier option is activated,
				- Matlab Compiler Runtime if the includeClassifier option is activated.
			It will also link some folders and give read-write-execute access to others.
			A specific version of FFmpeg is also installed locally (it will not overwrite any previously installed FFmpeg).
			
			
			Options:
			   --includeClassifier activate the classifiers. It will download and install Matlab CR as well a set of classifiers for a total weight of more than 1Gb.
			   --includeOnTheFlyClassifier activate the on the fly classifiers (cpuvisor). It will download and compile a lot of libraries and data for a total weight of more than 2Gb.
			   --includeFFmpegGPL activate GPL and h264 compilation flags for FFmpeg.
			   --nonInteractiveMode to assume yes/true to all question asked during installation (but Java installation confirmation than cannot be skipped).
			 
			 > ./install.sh --includeClassifier --includeOnTheFlyClassifier --includeFFmpegGPL --nonInteractiveMode
			 
	3) Wait between 10 minutes to 3 hours depending on the bandwidth of your Internet connection, the performance of your computer and the activated options.



After the installation start OpenAXES in a terminal using the WebLab Platform Launcher:
	> ./openaxes.sh start


You can then access OpenAXES locally on http://localhost/openaxes using the login 'axes' and password 'axes'.


You can stop OpenAXES using the command:
	> ./openaxes.sh stop



If you want to process new data, you have to move video files in the data/toIndex folder. The metadata could be provided aside.
Automatic Speech Recognition is done in English by default.
If your data is in French you can either provide a metadata file that states 'fr' in language metadata, or create a folder named fr inside to index folder and put your data in.
The folder data/toIndex-sample contains some video samples with associated metadata that can be used as bootstrap samples.  
The best is to do that prior to start the openaxes system to prevent from polling an unfinished file.


The process might last around 5 times the duration of the video.

The Hawtio Console available at http://localhost:8282/hawtio (login/pass: weblab/weblab) can be used to monitor the process.  


If you want to expose your video on a specific ip (instead of localhost),
edit conf/limas/cOpenAxes.py and URL_PREFIX line with "http://<your-ip>/" and restart jetty 
	> ./openaxes.sh jetty restart


Know issues and evolution requests at https://jira.ow2.org/issues/?jql=project%20%3D%20OPENAXES%20AND%20resolution%20%3D%20Unresolved%20ORDER%20BY%20priority%20DESC




The version 1.1.0 of the Open AXES is available for download at http://forge.ow2.org/project/showfiles.php?group_id=436.

Open AXES is an output of the AXES European project.
The goal AXES (http://axes-project.eu) was to develop tools that provide various types of users with new engaging ways to interact with audiovisual libraries,
helping them to discover, browse, search and enrich video archives. Based on the existing OW2 WebLab (http://weblab-project.org) integration platform for multimedia processing,
the "Open AXES" solution gathers innovative audiovisual content analyses technologies (shot and keyframes detection, image classification, speech transcription, large scale indexing, etc.)
as well as an ergonomic interface to navigate the video archive.


This version 1.1.0 contains:
	- a folder gathering service, provided by Airbus Defence and Space, enabling to collect video files and metadata (in various format: json, properties, rdf) in a set of relevant folders;
	- a shot-detector and keyframe-extraction service provided by Technicolor and enabling to select some keyframes representative of each shot of the video
 		(and prevent image based components to analyse every single frame of a video or picking random ones) which is available from binaries and only allowed for non commercial use;
 	- a video-normaliser service, provided by Airbus Defence and Space and based on FFmpeg, enabling to convert the original video in appropriate formats for other components and UI;
	- an image-classifier service, provided by the Katholieke Universiteit Leuven in charge of classifying selected images over a set of more than 1000 classes 
 		which is available from binaries and allowed for non commercial use only;
	- an automated speech recognition service, provided by Airbus Defence and Space and based on CMU Sphinx, extracting the text from English or French (using LIUM models) audio speech;
	- a named entity detection service, provided by Airbus Defence and Space and based on GATE platform, extracting organisation, persons and locations from the text of the speeches;
	- a text and metadata indexing service as well as the search and fuse search interfaces, named LIMAS and provided by the University of Twente,
		saving the results of the processes and being the backend server for the UI, also in charge of calling the other search engines and fusing their result.
		It is provided with an Apache 2 licence. The codebase is available at: https://bitbucket.org/alyr/limas/;
	- a near-duplicate image search service, provided by Airbus Defence and Space and based on the open source project Pastec;
	- an on-the-fly visual analysis service, provided by the University of Oxford and enabling to search inside a database of images using words, without having a predefined set of classifiers.
		It learns upon request a model by gathering positive examples from well-known Web search engines and looks up for similar images inside its own database. The codebase is available at
		https://github.com/kencoken/cpuvisor-srv with an Apache 2 licence.
	- a thin client web interface provided by the Dublin City University enables to search inside text and metadata to get the indexed video,
		to search for near duplicate images either from indexed video keyframes or for any user uploaded image and to search for images matching a visual concept
		(either using a pre-trained or by learning on the fly the concept).	It enables various operations over the videos like virtual cutting, collection management
		and social sharing (like/dislike). The codebase is available at https://github.com/kevinmcguinness/axes-research using an Apache 2 licence.
	- two technical components are also needed to let Visor and Limas works together. Jcpuvisor is the Java interface to Visor.
		It is available at https://bitbucket.org/alyr/jcpuvisor and provided as open source using an Apache 2 licence.
		Imsearch-tools is available at https://github.com/kencoken/imsearch-tools and provided with an Apache 2 licence. Please note however that this later project
		might be enforcing Google Terms of Use as well as authoring and image rights when gathering images on Google to send to Visor in order to learn its models.   

All together are packaged into an OW2 WebLab application. When not specified, the licence that apply on the Open AXES code is the LGPL-V2.1.

